# 4. Business & Marketing

## 4.1 Objectives

We know different types of cooling systems. From friges to ice boxes. What makes our product so unique is that with the portable mini cooler that we have designed you can go anywhere in Suriname without having to worry that the food, drink, medication, etc that you take with you will eventually be exposed to high temperatures. This, because with our mini cooler you can set the desired temperature with your mobile device and also monitor it. It works with solar power and has back up batteries, so no stress at all. Just carry it along with your favorate and / or important stuff that needs to be cooled at a certain temperature. It's a great solution to very soon transport the covid-19 vaccines to all little parts of Suriname, because the vaccines need to be cooled at a certain constant temperature, usually between 2 and 8 degrees C.


## 4.3 Ananlyze your market & segments

It's perfect for mainly water transport to the interior of Suriname, but also a good outcome for hikers, bikers, travelers, etc.
We can design different sizes of our cooling system. These will be offered for sale in the relevant shops.


## 4.4 Build your business canvas

<With the help or (Canvanizer Online)[https://canvanizer.com/choose/business-model-canvases] create a business canvas for your project>

## 4.5 Build your pitch deck

Everyone knows that we are in a covid pandamic and the vaccine is underway and needs to be transported in a cool environment.
In Suriname the vaccines needs to be transported throughout the country in villages and the interior. This is why we came with the idea to build a portable mini cooler which will be perfect for the transport of the vaccines. 
Another good purpose of this product will be for cooling drinks and more for hikers, bikers, travelers and many more.

1. Build your Problem Solution Pitch for Idea Pitching.

2. Build pitch deck for products/investor pitch.

## 4.6 Make a project Poster

This is our preliminary project poster:
![alt text](img/Project_poster.jpg)
<Build a poster for your project describing in a nutshell what it is for expo purposes>


## 4.7 Files & Code

[Crosslink to code](files/sketch_metWeb.rar)
<Add your embedded code and other relevant documents in a ZIp file under the /files folder and link here.>

## 4.8 References & Credits

<add all used references & give credits to those u used code and libraries from>