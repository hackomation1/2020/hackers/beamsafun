# Notes

## Part list:
### Peltier module 
- qty.: 2
- Type: TEC1- 12706
- [Link](https://pt.aliexpress.com/item/32320264423.html?spm=a2g03.search0302.3.17.1c6c3f02S4KEhT&ws_ab_test=searchweb0_0,searchweb201602_0,searchweb201603_0,ppcSwitch_0&algo_pvid=2539b4fe-ef18-4357-9c63-2117e716304f&algo_expid=2539b4fe-ef18-4357-9c63-2117e716304f-2)

### Thermal paste
- Specs: High thermal conductivity
- [Link](https://www.amazon.com/dp/B011F7W3LU?tag=amazongs-20)
### Power supply 12V

### 2 x Intel CPU's cooler

### Fans
-qty.: 2 x 92.24mm fan
- [Link](https://www.aliexpress.com/item/33021559018.html?spm=a2g0o.detail.1000014.45.5fa655741XlVLB&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.14976.158757.0&scm_id=1007.14976.158757.0&scm-url=1007.14976.158757.0&pvid=58a7c0e3-34df-4299-a131-0409f1e727d0&_t=gps-id:pcDetailBottomMoreOtherSeller,scm-url:1007.14976.158757.0,pvid:58a7c0e3-34df-4299-a131-0409f1e727d0,tpp_buckets:668%230%23131923%232_668%23808%234093%23719_668%23888%233325%2312_4976%230%23158757%230_4976%232711%237538%23611_4976%233104%239653%232_668%232846%238110%23330_668%232717%237563%23573)

### Styrofoam
- Specs: Thickness >30mm

### Solar panel:
- [Link](https://www.amazon.com/ECO-WORTHY-Solar-Panel-Watt-Module/dp/B00OZC19AY/ref=pd_sbs_86_6/136-0630828-1838756?_encoding=UTF8&pd_rd_i=B00OZC3X1C&pd_rd_r=2e5af896-6791-426d-a63d-e67cf988557b&pd_rd_w=YAUIe&pd_rd_wg=sgmIV&pf_rd_p=12b8d3e2-e203-4b23-a8bc-68a7d2806477&pf_rd_r=YYPZ76T9E6KX9GDY5Q2B&refRID=YYPZ76T9E6KX9GDY5Q2B&th=1)
### Solar Panel Controller :
- [Link](https://www.amazon.com/DFROBOT-900mA-Solar-Panel-Controller/dp/B07MML4YJV/ref=pd_bxgy_img_3/136-0630828-1838756?_encoding=UTF8&pd_rd_i=B07MML4YJV&pd_rd_r=bd955eeb-e0cb-413b-bdba-048a4dd27c21&pd_rd_w=vbmsv&pd_rd_wg=E0NBj&pf_rd_p=4e3f7fc3-00c8-46a6-a4db-8457e6319578&pf_rd_r=ZWF2CZSZH6TSHMBM2NZE&psc=1&refRID=ZWF2CZSZH6TSHMBM2NZE)
### Solar Power Management Module:
- [Link](https://www.amazon.com/Solar-Power-Management-Connection-Protection/dp/B07PBRK8KG/ref=pd_sbs_86_3/136-0630828-1838756?_encoding=UTF8&pd_rd_i=B07PBRK8KG&pd_rd_r=d598b71d-2aa2-4e88-b47e-4c2bf590dde1&pd_rd_w=pKtBh&pd_rd_wg=ccsnc&pf_rd_p=12b8d3e2-e203-4b23-a8bc-68a7d2806477&pf_rd_r=C5W47TVH6Y9HYNDXESZC&psc=1&refRID=C5W47TVH6Y9HYNDXESZC)
### Heatsink
-qty.: 2 x 40x40mm:
- [Link](https://www.aliexpress.com/item/32823612975.html?spm=a2g0o.detail.1000014.27.167310d6WtzE9A&gps-id=pcDetailBottomMoreOtherSeller&scm=1007.14976.158757.0&scm_id=1007.14976.158757.0&scm-url=1007.14976.158757.0&pvid=b3d85dfb-8016-495e-b837-0012c15ea917&_t=gps-id:pcDetailBottomMoreOtherSeller,scm-url:1007.14976.158757.0,pvid:b3d85dfb-8016-495e-b837-0012c15ea917,tpp_buckets:668%230%23131923%2320_668%23808%234093%23719_668%23888%233325%2312_4976%230%23158757%230_4976%232711%237538%23611_4976%233104%239653%237_668%232846%238110%23330_668%232717%237563%23573)
### 4 Channel Solid State Relay: 75.15x50.05x16.67mm
- [Link](https://www.aliexpress.com/item/32888878613.html?spm=a2g0o.productlist.0.0.1e7060faDDJFiF&algo_pvid=41ad5ea5-4c83-4c90-9c6c-1350db23c1b3&algo_expid=41ad5ea5-4c83-4c90-9c6c-1350db23c1b3-1&btsid=0ab6d70515913834218613112e0c84&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)
### 1 x ESP32 device kitC

### 1 x battery ESP21 device kitC

### 1 x thermister type TMP36

### 1 x  led light



