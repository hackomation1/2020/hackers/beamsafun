# 3. Front-End & Platform

THe front-end would consist of a webserver, which would host a website where the temperature would be controlled and monitored.

## 3.1 Objectives

When storing heat sensitive items, even the a slightest change in temperature can degrade the lifetime of said item. Here the use of distant monitoring through use of the dashboard hosted on the controller, gives the user the freedom to control and monitor the cooling capacity.


## 3.3 Steps taken

Directly programmed in arduino.c file.

## 3.4 Testing & Problems
* Relay problem: The relays used weren't getting inough voltage supplied by the esp32. Testing the relay wouldn't always work, so we where handed a solid state relay to test out. The solid state relay also lacked running consistency.
* Limited functionality of Arduino IDE compared to other normal wb/ app IDE's.
* Coupling of back-end and front-end is non-existent during programming. All in 1 file, which caused issues during troubleshooting and debugging.
* Limited documentation about web services of the esp32.



<provide tests performed, describe problems encountered and how they were solved (or not)>

## 3.5 Proof of work

Cool-E dashboard <https://youtu.be/U-BIJVa39RA>

## 3.6 Files & Code
[Crosslink to code](files/sketch_metWeb.rar)
<Add your embedded code and other relevant documents in a ZIp file under the /files folder and link here.>

## 3.7 References & Credits

<add all used references & give credits to those u used code and libraries from>