# 1. Portable mini cooler

We designed a portable mini cooler. It works just like a portable thermoelectric refrigerator with temperature indicating/altering features. That's a mini fridge/cooler that can be used to carry heat-sensitive items. 

* Solid-state construction features no moving parts resulting in higher reliability.
* Units can be mounted in any orientation.
* Devices are environmentally friendly because they use no CFCs and electrical noise is minimal.


## Overview & Features


Features:

* Cooling of items at set temperatures
* Temperature-control and Temperature-monitoring from a portable device that can connect to the server the dashboard is hosted on.


Special functions of our system:

* Temperature measerument done by an analog-temperature sensor.
* Temperature-control and Temperature-monitering through the use of a webpage.
* The controller attached to the coolbox will display the ip. Users will have to connect to the displayed ip, in order for them to control and moniter the temperature.


## Demo / Proof of work

### Project pictures:

Half manufactured cooler:

![Half manufactured cooler](img/Cooling_system_under_construction.jpg)

Test setup:

![Test setup](img/2.jpg)

Minimum temperature test:

![Minimum temperature test](img/Cooling.jpg)

### Demonstration of the working principle

Please check de link below for our proof of work:
[Proof of work](https://www.youtube.com/watch?v=A54F--3t4Xs)



## Deliverables

Construction of the project was completed on November 27.

- Project poster:

![Project poster](img/4.jpg)


- Proof of concept working:
- Our idea has been realized for relatively small cool boxes. We reach a minimum temperature of 10 degrees C. 
The cooling temperature is inversely proportional to the dimensions of the system components.

- Product pitch deck:
- We supply cooling systems for existing cool boxes. If you want to cool things like goods, products, etc. at a constant temperature, then this system is very suitable for it. This system is very suitable for the transport of mainly foodstuffs and medicines that must be kept at a certain constant temperature. Based on what we have designed, we can design larger or smaller cooling systems.

## Project landing page/website (optional)

No website has been created yet.


## The team & contact

We are team B.E.A.M.S.A.F.U.N. from the Anton de Kom University of Suriname, field of study Mechanical engineering.

The team members and their functions are as followed:

- Djokarto Xeyvan as Technical manager
- Hansildaar Rieciel as Marketing manager
- Harkhoe Dinesh as Software manager
- Kanhai Ajay as Technical manager
- Khoesial Sharaghani as Adminstrative manager
- Manna Nikaash as Project manager
