# 2. Embedded solution

<Describe the embedded solution for your project here.>

## 2.1 Objectives

Our main objective is cooling to a certain temperature and keeping that temperature at a constant level. That's why we decided to design a cooling system for existing ice boxes. We used different components. The peltier and the cooling fin are two of the important components. The size of the cooling fin determines how far the peltier can cool down. So, the size of the cooling fin and the temperature of the peltier are inversely related to each other.
<Provide background and reasoning what needs to be solved here, why this solution was choosen and what steps need to be taken>


## 2.3 Steps taken
* Step 1: Select an insulated system to which the cooling module can be attached to.
* Step 2: Add more insulation if needed.

![No shot bucko](img/Insulation.jpg)

* Step 3: Create the module.
![Okay dude](img/Module.jpeg)

* Step 4: Test the module's cooling capacity.

![ForsenCD](img/Cooling.jpg)

* Step 5: Make use of a sensor with high accuracy, which in our case was a analog temperature sensor.
![Swag](img/Air_circulation_fans_thermistor.jpg)

* Step 6: Using a relay as a switch that toggles the cooling module on/off.

![alt text](img/Components_2.jpg)

* Step 7: Write a program that:
  * Gives the user access to set the temperature to which the cooler should cool to.
  * Is capable of simultaneously measuring the temperature and toggling the relay switch.
* Step 8: Put it all together and run the system.
[System operating](https://youtu.be/qj3BG4WePZk)

<go through the proposed steps and describe and add images>

## 2.4 Testing & Problems

We performed three tests:
1. Resistance test on voltage change
2. Temperature test with one cooler
3. We determined the voltage at which the peltier has the lowest possible temperature on the hot side due to the used cooling fin, which ensures sufficient cooling

The problems we encountered were:
1. ESP32 could not drive simple components, for example relay.
2. Isolating the coolbox was also a problem we ran into.

## 2.5 Proof of work

Here are some photos of our components and work:

The circuit:

![alt text](img/Circuit.jpg)

![Air_circulation_fans](img/Air_circulation_fans.jpg)

Some of the important components are shown below:

![alt text](img/Components_1.jpg)

![alt text](img/Components_3.jpg)


The temperature of the water after cooling a while:

![alt text](img/The_temperature_of_the_water_after_cooling_a_while.jpg)


## 2.6 Files & Code
[Crosslink to code](files/sketch_metWeb.rar)
<Add your embedded code and other relevant documents in a ZIp file under the /files folder and link here.>

## 2.7 References & Credits

<add all used references & give credits to those u used code and libraries from>